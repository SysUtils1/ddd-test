using System;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.DomainModelLayer.IdentifyManagement;

namespace BodyLeasing.Helpers.Repository
{
    public interface IUserRepository
    {
        User FindById(Guid id);
        User FindByUsername(string email);
        void Add(User user);
        void Remove(User user);
    }
}