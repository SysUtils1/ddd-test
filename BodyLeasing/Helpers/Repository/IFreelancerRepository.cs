using System;
using System.Collections.Generic;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.Helpers.Specification;

namespace BodyLeasing.Helpers.Repository
{
    public interface IFreelancerRepository
    {
        Freelancer FindById(Guid id);
        IEnumerable<Freelancer> Find(ISpecification<Freelancer> spec);
        void Add(Freelancer freelancer);
        void Remove(Freelancer freelancer);
    }
}