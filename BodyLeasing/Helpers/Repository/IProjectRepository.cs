using System;
using System.Collections.Generic;
using BodyLeasing.DomainModelLayer.ProjectManagement;

namespace BodyLeasing.Helpers.Repository
{
    public interface IProjectRepository
    {
        Project FindById(Guid id);
        IEnumerable<Project> FindByCustomerId(Guid id);
        void Add(Project freelancer);
        void Remove(Project freelancer);
    }
}