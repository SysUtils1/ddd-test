using System;
using BodyLeasing.DomainModelLayer.CustomerManagement;

namespace BodyLeasing.Helpers.Repository
{
    public interface ICustomerRepository
    {
        Customer FindById(Guid id);
        void Add(Customer freelancer);
        void Remove(Customer freelancer);
    }
}