using System;

namespace BodyLeasing.Helpers.Domain
{
    public interface IAggregateRoot
    {
        Guid Id { get; }
    }
}