using System;
using System.Collections.Generic;
using System.Linq;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.Helpers.Repository;
using BodyLeasing.Helpers.Specification;

namespace BodyLeasing.InfrastructureLayer
{
    public class FreelancerMemoryRepository: IFreelancerRepository
    {
        private static readonly List<Freelancer> entities = new List<Freelancer>();

        public Freelancer FindById(Guid id)
        {
            return entities.FirstOrDefault(x => x.Id == id);
        }
        
        public IEnumerable<Freelancer> Find(ISpecification<Freelancer> spec)
        {
            return entities.Where(spec.IsSatisfiedBy);
        }

        public void Add(Freelancer freelancer)
        {
            entities.Add(freelancer);
        }

        public void Remove(Freelancer freelancer)
        {
            entities.Remove(freelancer);
        }
    }
}