using System;
using System.Collections.Generic;
using System.Linq;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.DomainModelLayer.IdentifyManagement;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.InfrastructureLayer
{
    public class UserRepository: IUserRepository
    {
        private static readonly List<User> entities = new List<User>();
        
        public User FindById(Guid id)
        {
            return entities.FirstOrDefault(x => x.Id == id);
        }

        public User FindByUsername(string username)
        {
            return entities.FirstOrDefault(x => x.Username == username);
        }

        public void Add(User user)
        {
            entities.Add(user);
        }

        public void Remove(User user)
        {
            entities.Remove(user);
        }
    }
}