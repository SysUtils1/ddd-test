using System;
using System.Collections.Generic;
using System.Linq;
using BodyLeasing.DomainModelLayer.CustomerManagement;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.InfrastructureLayer
{
    public class CustomerRepository: ICustomerRepository
    {
        private static readonly List<Customer> entities = new List<Customer>();

        public Customer FindById(Guid id)
        {
            return entities.FirstOrDefault(x => x.Id == id);
        }

        public void Add(Customer сustomer)
        {
            entities.Add(сustomer);
        }

        public void Remove(Customer сustomer)
        {
            entities.Remove(сustomer);
        }
    }
}