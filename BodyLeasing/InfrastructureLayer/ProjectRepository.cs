using System;
using System.Collections.Generic;
using System.Linq;
using BodyLeasing.DomainModelLayer.ProjectManagement;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.InfrastructureLayer
{
    public class ProjectRepository: IProjectRepository
    {
        private static readonly List<Project> entities = new List<Project>();
        
        public Project FindById(Guid id)
        {
            return entities.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Project> FindByCustomerId(Guid customerId)
        {
            return entities.FindAll(x => x.CustomerId == customerId);
        }

        public void Add(Project project)
        {
            entities.Add(project);
        }

        public void Remove(Project project)
        {
            entities.Remove(project);
        }
    }
}