namespace BodyLeasing.DomainModelLayer.IdentifyManagement
{
    public enum Role
    {
        Freelancer, Customer, Employer
    }
}