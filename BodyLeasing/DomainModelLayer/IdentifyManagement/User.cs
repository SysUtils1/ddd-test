using System;
using System.Security.Cryptography;
using System.Text;

namespace BodyLeasing.DomainModelLayer.IdentifyManagement
{
    public class User
    {
        public Guid Id { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public Role Role { get; private set; }

        public User(string username, string password)
        {
            this.Id = Guid.NewGuid();
            var shaM = new SHA512Managed();
      
            this.Username = username;
            var hash = shaM.ComputeHash(Encoding.UTF8.GetBytes(password));
            var passBuilder = new StringBuilder();
            foreach (var i in hash)
            {
                passBuilder.Append(hash[i].ToString("X2"));
            }

            this.Password = passBuilder.ToString();
        }

        public void ChangeUsername(string newUsername)
        {
            this.Username = newUsername;
        } 
    }
}