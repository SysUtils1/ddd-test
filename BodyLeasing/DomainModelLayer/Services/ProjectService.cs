using System;
using System.Linq;
using BodyLeasing.Helpers.Repository;
using Microsoft.AspNetCore.Mvc;

namespace BodyLeasing.DomainModelLayer.Services
{
    public class ProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            this._projectRepository = projectRepository;
        }

        public bool ProjectExistsForCustomer(Guid customerId)
        {
            return _projectRepository.FindByCustomerId(customerId).Any();
        }
    }
}