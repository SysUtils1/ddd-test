using System;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.DomainModelLayer.Services
{
    public class UserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }

        public bool UserExists(Guid userId)
        {
            return _userRepository.FindById(userId) != null;
        }
    }
}