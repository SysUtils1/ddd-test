using System;

namespace BodyLeasing.DomainModelLayer.ProjectManagement
{
    public class Project
    {
        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public Guid CustomerId { get; protected set; }

        public Project(Guid customerId, string name)
        {
            this.CustomerId = customerId;
            this.Name = name;
        }
    }
}