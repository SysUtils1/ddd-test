using System;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.Helpers.Domain;

namespace BodyLeasing.DomainModelLayer.CustomerManagement
{
    public class Customer: IAggregateRoot
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }

        public Customer(string name)
        {
            this.Id = Guid.NewGuid();
            this.Name = name;
        }
    }
}