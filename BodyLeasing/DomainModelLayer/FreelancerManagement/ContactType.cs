namespace BodyLeasing.DomainModelLayer.FreelancerManagement
{
    public enum ContactType
    {
        Fax, Mail, Mobile
    }
}