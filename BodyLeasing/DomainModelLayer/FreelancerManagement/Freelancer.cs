using System;
using BodyLeasing.Helpers.Domain;

namespace BodyLeasing.DomainModelLayer.FreelancerManagement
{
    public class Freelancer: IAggregateRoot
    {
        public Guid Id { get; private set; }
        public Address Address { get; set; }
        public Guid UserId;
        public CommunicationChannel[] CommunicationChannels;

        public Freelancer(Guid userId, Address address)
        {
            this.Id = Guid.NewGuid();
            this.UserId = userId;
            this.Address = address;
        }
    }
}