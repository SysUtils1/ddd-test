using System;
using BodyLeasing.Helpers.Domain;

namespace BodyLeasing.DomainModelLayer.FreelancerManagement
{
    public class Timesheet: IAggregateRoot
    {
        public Guid Id { get; private set; }
        public DateTime Date { get; private set; }
        public int Hours { get; private set; }

        public Timesheet(DateTime date, int hours)
        {
            this.Id = Guid.NewGuid();
            this.Date = date;
            this.Hours = hours;
        }
    }
}