using System;
using System.Linq.Expressions;
using System.Reflection.Metadata.Ecma335;
using BodyLeasing.Helpers.Specification;

namespace BodyLeasing.DomainModelLayer.FreelancerManagement
{
    public class AllFreelancersSpec: ISpecification<Freelancer>
    {
        public Expression<Func<Freelancer, bool>> SpecExpression { get; }
        public bool IsSatisfiedBy(Freelancer obj)
        {
            return true;
        }
    }
}