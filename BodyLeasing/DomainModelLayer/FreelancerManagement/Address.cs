namespace BodyLeasing.DomainModelLayer.FreelancerManagement
{
    public struct Address
    {
        public string Line1;
        public string Line2;
        public string ZipCode;
        public string City;
    }
}