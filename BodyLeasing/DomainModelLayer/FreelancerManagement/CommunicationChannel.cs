namespace BodyLeasing.DomainModelLayer.FreelancerManagement
{
    public class CommunicationChannel
    {
        public string Value { get; private set; }
        public ContactType Type { get; private set; }
    }
}