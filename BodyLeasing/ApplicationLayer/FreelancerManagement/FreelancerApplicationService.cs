using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.DomainModelLayer.Services;
using BodyLeasing.Helpers.Repository;
using BodyLeasing.Helpers.Specification;
using BodyLeasing.InfrastructureLayer;

namespace BodyLeasing.ApplicationLayer.FreelancerManagement
{
    public class FreelancerApplicationService: IFreelancerApplicationService
    {
        private readonly IFreelancerRepository _freelancerRepository;
        private readonly UserService _userService;

        public FreelancerApplicationService(IFreelancerRepository freelancerRepository, UserService userService)
        {
            this._freelancerRepository = freelancerRepository;
            this._userService = userService;
        }
        
        public FreelancerDto Add(FreelancerDto freelancerDto)
        {
            if (!this._userService.UserExists(freelancerDto.UserId))
                throw new Exception($"User was not found with this Id: {freelancerDto.UserId}");
            
            var freelancer = new Freelancer(freelancerDto.UserId, freelancerDto.Address);
            _freelancerRepository.Add(freelancer);
            return Mapper.Map<Freelancer, FreelancerDto>(freelancer);
        }
        
        public FreelancerDto Get(Guid id)
        {
            var freelancer = _freelancerRepository.FindById(id);
            ValidateFreelancer(id, freelancer);
            return Mapper.Map<Freelancer, FreelancerDto>(freelancer);
        }

        public IEnumerable<FreelancerDto> Get()
        {
            var freelancers = _freelancerRepository.Find(new AllFreelancersSpec());

            return freelancers.Select(x => Mapper.Map<Freelancer, FreelancerDto>(x));
        }


        public FreelancerDto Update(FreelancerDto freelancerDto)
        {
            if (freelancerDto.Id == Guid.Empty)
                throw new Exception("Id can't be empty");
            var freelancer = _freelancerRepository.FindById(freelancerDto.Id);
            ValidateFreelancer(freelancerDto.Id, freelancer);
            freelancer.Address = freelancerDto.Address;
            return Mapper.Map<Freelancer, FreelancerDto>(freelancer);
        }
        
        private static void ValidateFreelancer(Guid id, Freelancer freelancer)
        {
            if (freelancer == null)
                throw new Exception($"Freelancer was not found with this Id: {id}");
        }
    }
}