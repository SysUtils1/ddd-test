using System;
using System.Collections.Generic;

namespace BodyLeasing.ApplicationLayer.FreelancerManagement
{
    public interface IFreelancerApplicationService
    {
        FreelancerDto Add(FreelancerDto freelancerDto);
        FreelancerDto Get(Guid id);
        IEnumerable<FreelancerDto> Get();
        FreelancerDto Update(FreelancerDto freelancerDto);
    }
}