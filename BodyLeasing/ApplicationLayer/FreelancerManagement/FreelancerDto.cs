using System;
using BodyLeasing.DomainModelLayer.FreelancerManagement;

namespace BodyLeasing.ApplicationLayer.FreelancerManagement
{
    public class FreelancerDto
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Address Address { get; set; }
        public CommunicationChannel[] CommunicationChannels { get; set; }
    }
}