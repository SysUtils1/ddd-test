using System;

namespace BodyLeasing.ApplicationLayer.CustomerManagement
{
    public class CustomerDto
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
    }
}