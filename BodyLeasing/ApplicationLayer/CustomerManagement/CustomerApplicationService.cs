using System;
using System.Linq;
using AutoMapper;
using BodyLeasing.DomainModelLayer.CustomerManagement;
using BodyLeasing.DomainModelLayer.Services;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.ApplicationLayer.CustomerManagement
{
    public class CustomerApplicationService: ICustomerApplicationService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ProjectService _projectService;
        
        public CustomerApplicationService(ICustomerRepository customerRepository, IUserRepository userRepository, IProjectRepository projectRepository, ProjectService projectService)
        {
            this._customerRepository = customerRepository;
            this._projectService = projectService;
           
        }

        public CustomerDto Add(CustomerDto customerDto)
        {
            var customer = new Customer(customerDto.Name);
            _customerRepository.Add(customer);
            return Mapper.Map<Customer, CustomerDto>(customer);
        }
        
        public CustomerDto Get(Guid id)
        {
            var customer = _customerRepository.FindById(id);
            if (customer == null)
                throw new Exception($"Customer was not found with this Id: {id}");
            return Mapper.Map<Customer, CustomerDto>(customer);
        }
        
        public CustomerDto Remove(CustomerDto customerDto)
        {
            var customer = _customerRepository.FindById(customerDto.Id);
            if (customer == null)
                throw new Exception($"Customer was not found with this Id: {customerDto.Id}");
               
            if (this._projectService.ProjectExistsForCustomer(customer.Id))
                throw new Exception($"Customer has one or more projects");
            _customerRepository.Remove(customer);
            return Mapper.Map<Customer, CustomerDto>(customer);
        }
    }
}