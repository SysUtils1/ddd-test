using System;

namespace BodyLeasing.ApplicationLayer.CustomerManagement
{
    public interface ICustomerApplicationService
    {
        CustomerDto Add(CustomerDto customerDto);
        CustomerDto Get(Guid id);
        CustomerDto Remove(CustomerDto customerDto);
    }
}