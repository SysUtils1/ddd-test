using System;

namespace BodyLeasing.ApplicationLayer.ProjectManagement
{
    public class ProjectDto
    {
        public Guid Id;
        public string Name;
        public Guid CustomerId;
    }
}