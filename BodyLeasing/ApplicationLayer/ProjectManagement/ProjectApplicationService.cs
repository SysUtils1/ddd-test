using System;
using AutoMapper;
using BodyLeasing.DomainModelLayer.ProjectManagement;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.ApplicationLayer.ProjectManagement
{
    public class ProjectApplicationService: IProjectApplicationService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectApplicationService(IProjectRepository projectRepository, IUserRepository userRepository)
        {
            this._projectRepository = projectRepository;
        }

        public ProjectDto Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public ProjectDto Add(ProjectDto projectDto)
        {
            var project = new Project(projectDto.CustomerId, projectDto.Name);
            _projectRepository.Add(project);
            
            return Mapper.Map<Project, ProjectDto>(project);
        }

        public ProjectDto Remove(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}