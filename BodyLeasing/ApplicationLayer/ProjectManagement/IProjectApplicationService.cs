using System;

namespace BodyLeasing.ApplicationLayer.ProjectManagement
{
    public interface IProjectApplicationService
    {
        ProjectDto Get(Guid id);
        ProjectDto Add(ProjectDto project);
        ProjectDto Remove(Guid id);
    }
}