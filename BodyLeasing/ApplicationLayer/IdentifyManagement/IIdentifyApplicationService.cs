using System;

namespace BodyLeasing.ApplicationLayer.IdentifyManagement
{
    public interface IIdentifyApplicationService
    {
        UserDto Get(Guid id);
        UserDto GetByUsername(string username);
        UserDto Add(string username, string password);
        UserDto Remove(UserDto userDto);
    }
}