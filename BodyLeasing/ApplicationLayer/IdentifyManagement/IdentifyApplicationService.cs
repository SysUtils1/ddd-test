using System;
using AutoMapper;
using BodyLeasing.ApplicationLayer.FreelancerManagement;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.DomainModelLayer.IdentifyManagement;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.ApplicationLayer.IdentifyManagement
{
    public class IdentifyApplicationService: IIdentifyApplicationService
    {
        private readonly IUserRepository _userRepository;
        
        public IdentifyApplicationService(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        
        public UserDto Add(string username, string password)
        {
            if (_userRepository.FindByUsername(username) != null)
                throw new Exception($"This Username already taken: {username}");

            var user = new User(username, password);
            _userRepository.Add(user);
            
            return Mapper.Map<User, UserDto>(user);
        }

        public UserDto Get(Guid id)
        {
            var user = _userRepository.FindById(id);
            
            return Mapper.Map<User, UserDto>(user);
        }
        
        public UserDto GetByUsername(string username)
        {
            var user = _userRepository.FindByUsername(username);
            
            return Mapper.Map<User, UserDto>(user);
        }

        public UserDto Remove(UserDto userDto)
        {
            var user = _userRepository.FindById(userDto.Id);
            if (user == null)
                throw new Exception($"User was not found with this Id: {userDto.Id}");
            
            _userRepository.Remove(user);
            
            return Mapper.Map<User, UserDto>(user);
        }
    }
}