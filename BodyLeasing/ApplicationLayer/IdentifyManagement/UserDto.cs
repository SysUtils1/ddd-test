using System;
using BodyLeasing.DomainModelLayer.IdentifyManagement;
using BodyLeasing.Helpers.Repository;

namespace BodyLeasing.ApplicationLayer.IdentifyManagement
{
    public class UserDto
    {
        public Guid Id { get; private set; }
        public string Username { get; private set; }
        public Role Role { get; private set; }
    }
}