﻿using System;
using BodyLeasing.ApplicationLayer.FreelancerManagement;
using BodyLeasing.DomainModelLayer.FreelancerManagement;
using BodyLeasing.InfrastructureLayer;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var freelancerMemoryRepository = new FreelancerMemoryRepository();
            var userRepository = new UserRepository();
            var freelancerService = new FreelancerApplicationService(freelancerMemoryRepository, userRepository);
            var addr = new Address {ZipCode = "test"};
            var freelancer = new FreelancerDto{Address = addr};
            var addedFreelancer = freelancerService.Add(freelancer);
        }
    }
}