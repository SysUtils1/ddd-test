using System;
using BodyLeasing.ApplicationLayer.FreelancerManagement;
using BodyLeasing.ApplicationLayer.IdentifyManagement;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.controllers
{
    public class UserController : Controller
    {
        public class Credentials
        {
            public string Username;
            public string Password;
        }
        
        private readonly IIdentifyApplicationService _identifyApplicationService;
        
        public UserController(IIdentifyApplicationService identifyApplicationService)
        {
            this._identifyApplicationService = identifyApplicationService;
        }
        
        [HttpPost]
        public Response<UserDto> Registration([FromBody]Credentials credentials)
        {
            var response = new Response<UserDto>();
            try
            {
                response.Object = _identifyApplicationService.Add(credentials.Username, credentials.Password);
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
    }
}