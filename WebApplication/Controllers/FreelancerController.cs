using System;
using System.Collections.Generic;
using BodyLeasing.ApplicationLayer.FreelancerManagement;
using BodyLeasing.ApplicationLayer.IdentifyManagement;
using Microsoft.AspNetCore.Mvc;
using WebApplication.Models;

namespace WebApplication.controllers
{
    public class FreelancerController : Controller
    {
        private readonly IFreelancerApplicationService _freelancerController;

        public FreelancerController(IFreelancerApplicationService freelancerApplicationService)
        {
            this._freelancerController = freelancerApplicationService;
        }
        
        [HttpGet]
        public Response<FreelancerDto> GetById(Guid id)
        {
            var response = new Response<FreelancerDto>();
            try
            {
                response.Object = _freelancerController.Get(id);
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
        
        [HttpGet]
        public Response<IEnumerable<FreelancerDto>> Get()
        {
            var response = new Response<IEnumerable<FreelancerDto>>();
            try
            {
                response.Object = _freelancerController.Get();
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
        
        [HttpPost]
        public Response<FreelancerDto> Add([FromBody]FreelancerDto freelancerDto)
        {
            var response = new Response<FreelancerDto>();
            try
            {
                response.Object = _freelancerController.Add(freelancerDto);
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
        
        [HttpPost]
        public Response<FreelancerDto> Update([FromBody]FreelancerDto freelancerDto)
        {
            var response = new Response<FreelancerDto>();
            try
            {
                response.Object = _freelancerController.Update(freelancerDto);
            }
            catch (Exception ex)
            {
                response.Errored = true;
                response.ErrorMessage = ex.Message;
            }
            return response;
        }
    }
}