﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BodyLeasing.ApplicationLayer.FreelancerManagement;
using BodyLeasing.ApplicationLayer.IdentifyManagement;
using BodyLeasing.DomainModelLayer.IdentifyManagement;
using BodyLeasing.DomainModelLayer.Services;
using BodyLeasing.Helpers.Repository;
using BodyLeasing.InfrastructureLayer;
using GraphiQl;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace WebApplication
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IFreelancerRepository, FreelancerMemoryRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IProjectRepository, ProjectRepository>();
            services.AddSingleton<ICustomerRepository, CustomerRepository>();
            services.AddSingleton<UserService, UserService>();
            services.AddSingleton<IFreelancerApplicationService, FreelancerApplicationService>();
            services.AddSingleton<IIdentifyApplicationService, IdentifyApplicationService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "/api/{controller}/{action}");
            });
        }
    }
}